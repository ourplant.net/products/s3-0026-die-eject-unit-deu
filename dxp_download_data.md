Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu/-/raw/main/01_operating_manual/S3-0026_A_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu/-/raw/main/01_operating_manual/S3-0026_A_operating%20manual.pdf)                    |
| assembly drawing         | [de](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu/-/raw/main/02_assembly_drawing/s3-0026_A_ZNB_deu.pdf)                 |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu/-/raw/main/03_circuit_diagram/S3-0026_EPLAN-A.pdf)                  |
| maintenance instructions |                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0026-die-eject-unit-deu/-/raw/main/05_spare_parts/S3-0026-EVL-A.pdf)                  |

